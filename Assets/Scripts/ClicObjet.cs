﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClicObjet : MonoBehaviour
{
    public Turn turn;
    public GameObject UIinfo;
    //public GameObject SoundManager;

    public int _objet;
    public int _son;
    public int _description;

    private SoundManagerScript SManager;

    //Camera zoom on object
    public float zoomDepth = 2.5f;
    private Quaternion initialRotation;
    private Vector3 objectInitialPosition;
    private Camera mainCamera;
    private bool isZoomed = false;
    public GameObject _Cube;
    public float _speedTrans = 3;
    public int goZoom = 0;
    public bool noRepeat = false;
    public GameObject back;
    CameraLimit cam;
    public ButtonsReal[] buttons;



    // Start is called before the first frame update
    void Start()
    {

        
        turn = GetComponent<Turn>();

        SManager = FindObjectOfType<SoundManagerScript>();

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        cam = mainCamera.GetComponent<CameraLimit>();

        objectInitialPosition = back.transform.position;
        initialRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        // Clic sur un objet

        if (Input.GetMouseButtonDown(0))                                                            // Esteban - A l'input d'un clic de souris.
        {
            RaycastHit hit;                                                                         // Esteban - Création d'un RayCast.
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);                            // Esteban - Utilisation du RayCast afin de vérifier si je clique sur un objet.

            if (Physics.Raycast(ray, out hit, 1000.0f))                                             // Esteban - Si le RayCast trouve un objet
            {
                if (hit.transform.tag == "Objet")                                                   // Esteban - Et que cet objet a le tag "objet"
                {
                    ClicObjet potato = hit.transform.gameObject.GetComponent<ClicObjet>();
                    UIinfo.SetActive(true);                                                         // Esteban - Alors j'active le boutton d'infos qui était invisible jusque là
                   // _son = potato._objet;               // Esteban - La variable _Son prend comme valeur celle de l'objet sur lequel nous avons cliqué

                    if (!potato.isZoomed && !cam.something)
                        potato.CameraZoom();                                                               // Quentin - La fonction Camera Zoom est appelée lorsqu'un objet si il ne l'est pas déjà
                    else
                        return;
                }
            }
        }

        if (goZoom == 1 && noRepeat == false)
        {
            ZoomFonc();
        }
    }

    public void ClicInfos()
    {
        SManager.InfosDialogue(_son);                                                               // Esteban - Je lance la fonction InfosDialogue du script SoundManager

    }

    public void ClicDescription()                                                                   //Tristan - Ajouté la description par le guide quand on clic sur le bouton demandé.
    {
        SManager.InfosDescription(_description);
    }

    public void QuitObject()
    {
        UIinfo.SetActive(false);                                                                    // Esteban - Bouton retour qui permet de désactiver tout l'UI d'infos de l'objet  
        SManager.QuitObjet();
    }

    public void CameraZoom()
    {
        foreach(ButtonsReal butt in buttons)
        {
            butt.actualObject = this;
        }
        cam.something = true;
        objectInitialPosition = transform.position;                                               // Quentin - La variable objectInitialPosition récupère la position de départ de l'objet
        gameObject.transform.position = mainCamera.ScreenToWorldPoint(new Vector3(0, 0, zoomDepth));// Quentin - L'objet est déplacé au centre de la caméra
        transform.SetParent(mainCamera.transform, false);                                           // Quentin - L'objet est placé en enfant de la caméra pour partager sa position
        isZoomed = true;
    }

    public void CameraUnzoom()
    {
        foreach (ButtonsReal butt in buttons)
        {
            butt.actualObject = null;
        }
        cam.something = false;
        transform.SetParent(null);                                                                  // Quentin - L'objet est séparé de la caméra
        transform.position = objectInitialPosition;                                        // Quentin - L'objet reprend sa position initiale
        transform.rotation = initialRotation;                                        // Quentin - L'objet reprend sa rotation initiale
        isZoomed = false;
    }

    IEnumerator Zoom()
    {
        this.gameObject.transform.Translate(2f * _speedTrans * Time.deltaTime, 0, 0);
        yield return new WaitForSeconds(0.1f);
        this.gameObject.transform.Translate(0, 0, 3f * _speedTrans * Time.deltaTime);
        yield return new WaitForSeconds(0.8f);
        goZoom = 0;
        noRepeat = true;
        turn.enabled = true;
    }

    public void ZoomFonc()
    {
        StartCoroutine(Zoom());
        
    }

    public void Back()
    {
        transform.position = objectInitialPosition;                                        
        transform.rotation = initialRotation;
        noRepeat = false;
    }
}
