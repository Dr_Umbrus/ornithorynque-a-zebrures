﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public AudioClip[] sons;

    AudioSource AudioSrc;
    private Guide guide;

    void Start()
    {
        guide = FindObjectOfType<Guide>();
        AudioSrc = GetComponent<AudioSource>();
    }

    public void InfosDialogue(int _son)
    {
        guide.Taire();
        AudioSrc.Stop();
        AudioSrc.clip = sons[_son];
        AudioSrc.Play();
    }

    public void QuitObjet() //Tristan - Relance le guide en quittant l'objet
    {
        guide.BackDialogue();
    }

    public void InfosDescription(int _son) // Tristan - Lance la description d'un objet par le guide. 
    {
        AudioSrc.Stop();
        guide.StartDescription(_son);
    }
}
