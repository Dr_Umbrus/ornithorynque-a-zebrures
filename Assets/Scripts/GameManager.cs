﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public List<TextAsset> _dialoguesNames;
    public List<TextAsset> _descriptionNames;
    public static GameManager _instance { get; private set; }

    //Liste des audios de narration. Dans l'ordre de lecture.
    public List<AudioClip> _dialogues;
    //Liste des audios de description (tenir une liste à jour);
    public List<AudioClip> _descriptions;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }

        _instance = this;
    }

    private void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void Guided(Guide guide)
    {
        guide._descriptionNames = _descriptionNames;
        guide._dialoguesNames = _dialoguesNames;
        guide._dialogues = _dialogues;
        guide._descriptions = _descriptions;
    }
}
