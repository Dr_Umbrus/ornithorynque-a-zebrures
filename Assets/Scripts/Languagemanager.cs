﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Languagemanager : MonoBehaviour
{
    //Le guide
    public  GameManager _guide;

    //_text = Sous titres de base. _desc = Sous titres des descriptions. _dial = dialogues du guide. _descVoc = description audio.
    string _currentAudio, _currentText;
    public List<TextAsset> _textFr;
    public List<TextAsset> _textEn;
    public List<AudioClip> _dialFr;
    public List<AudioClip> _descVocFr;
    public List<TextAsset> _descFr;
    public List<TextAsset> _descEn;
    public List<AudioClip> _dialEn;
    public List<AudioClip> _descVocEn;

    void Start()
    {
        _guide = FindObjectOfType<GameManager>();
    }
    
    //On change la langue et on adapte les noms du guide pour récupérer les bons fichiers.
    public void ChangeLanguage(string newLanguage)
    {
        _currentText = newLanguage;
        switch (newLanguage)
        {
            case "Francais":
                _guide._descriptionNames = _descFr;
                _guide._dialoguesNames = _textFr;

                break;
            case "Anglais":
                _guide._descriptionNames = _descEn;
                _guide._dialoguesNames = _textEn;

                break;
        }
    }

    public void ChangeAudio(string newLanguage)
    {
        _currentAudio = newLanguage;
        switch (newLanguage)
        {
            case "Francais":

                _guide._dialogues = _dialFr;
                _guide._descriptions = _descVocFr;
                break;
            case "Anglais":

                _guide._dialogues = _dialEn;
                _guide._descriptions = _descVocEn;
                break;
        }
    }

}
