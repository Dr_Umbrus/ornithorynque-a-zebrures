﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsReal : MonoBehaviour
{
    public ClicObjet actualObject;
    public int id;

    private void OnMouseDown()
    {
        if (id == 0)
        {
            actualObject.ClicDescription();
        }
        if (id == 1)
        {
            actualObject.ClicInfos();
        }
        if (id == 2)
        {
            actualObject.CameraUnzoom();
        }
    }
}
